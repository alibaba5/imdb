﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using imdb.Models;

namespace imdb.Data.EFCore
{
    public class EfCoreMovieRepository : EfCoreRepository<Movie, imdbContext>
    {
        public EfCoreMovieRepository(imdbContext context) : base(context)
        {

        }
    }
}
