﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace imdb.Models
{
    public class imdbContext : DbContext
    {
        public imdbContext (DbContextOptions<imdbContext> options)
            : base(options)
        {
        }

        public DbSet<imdb.Models.Movie> Movie { get; set; }
    }
}
