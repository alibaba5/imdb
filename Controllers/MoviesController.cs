﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using imdb.Models;
using imdb.Data.EFCore;
using imdb.Data;

namespace imdb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ImdbController<Movie, EfCoreMovieRepository>
    {
        private readonly imdbContext _context;

        public MoviesController(EfCoreMovieRepository repository) : base(repository)
        {

        }

    }
}
